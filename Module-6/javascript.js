//Grows the box by 150px
document.getElementById("button1").addEventListener("click", function() {
  document.getElementById("box").style.height = "300px";
  document.getElementById("box").style.width = "300px";
})
//Changes background color to blue
document.getElementById("button2").addEventListener("click", function() {
  document.getElementById("box").style.background = "blue";
})
//Opacity down to 10%?
document.getElementById("button3").addEventListener("click", function() {
  document.getElementById("box").style.opacity = ".1";
})
document.getElementById("button5").addEventListener("click", function() {
  document.getElementById("box").style.borderRadius = "50%";
})
//Reset aback to default values
document.getElementById("button4").addEventListener("click", function() {
  document.getElementById("box").style.opacity = "1";
  document.getElementById("box").style.height = "150px";
  document.getElementById("box").style.width = "150px";
  document.getElementById("box").style.background = "orange";
  document.getElementById("box").style.borderRadius = "0%"
})